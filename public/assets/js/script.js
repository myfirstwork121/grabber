$(document).ready(function() {
    $('#example-checkbox').DataTable({
        responsive: false,
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:0,
        } ],
        aLengthMenu: [15, 25, 50],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        order: [ 1, 'asc' ],
        bFilter: true,
        bLengthChange: true,
        pagingType: "simple",
        "paging": true,
        "searching": true,
        "language": {
            "info": " _START_ - _END_ of _TOTAL_ ",
            "sLengthMenu": "<span class='custom-select-title'>Rows per page:</span> <span class='custom-select'> _MENU_ </span>",
            "sSearch": "",
            "sSearchPlaceholder": "Search",
            "paginate": {
                "sNext": " ",
                "sPrevious": " "
            },
        },
        dom:
            "<'pmd-card-title'<'data-table-title'><'search-paper pmd-textfield'f>>" +
            "<'custom-select-info'<'custom-select-item'><'custom-select-action'>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'pmd-card-footer' <'pmd-datatable-pagination' l i p>>",
    });

    /// Select value
    $('.custom-select-info').hide();

    $('#example-checkbox tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            var rowinfo = $(this).closest('.dataTables_wrapper').find('.select-info').text();
            $(this).closest('.dataTables_wrapper').find('.custom-select-info .custom-select-item').text(rowinfo);
            if ($(this).closest('.dataTables_wrapper').find('.custom-select-info .custom-select-item').text() != null){
                $(this).closest('.dataTables_wrapper').find('.custom-select-info').show();
                //show delet button
            } else{
                $(this).closest('.dataTables_wrapper').find('.custom-select-info').hide();
            }
        }
        else {
            var rowinfo = $(this).closest('.dataTables_wrapper').find('.select-info').text();
            $(this).closest('.dataTables_wrapper').find('.custom-select-info .custom-select-item').text(rowinfo);
        }
        if($('#example-checkbox').find('.selected').length == 0){
            $(this).closest('.dataTables_wrapper').find('.custom-select-info').hide();
        }
    } );
    $(".data-table-responsive").html('<h2 class="pmd-card-title-text">Products</h2>');
    $(".custom-select-action").html('<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">delete</i></button><button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">more_vert</i></button>');

} );

let exampleDatatable = $('#example').DataTable({
    responsive: {
        details: {
            type: 'column',
            target: 'tr'
        }
    },
    columnDefs: [ {
        className: 'control',
        orderable: false,
        targets:   1
    } ],
    order: [ 1, 'asc' ],
    bFilter: true,
    bLengthChange: true,
    pagingType: "simple",
    "paging": true,
    "searching": true,
    "language": {
        "info": " _START_ - _END_ of _TOTAL_ ",
        "sLengthMenu": "<span class='custom-select-title'>Rows per page:</span> <span class='custom-select'> _MENU_ </span>",
        "sSearch": "",
        "sSearchPlaceholder": "Search",
        "paginate": {
            "sNext": " ",
            "sPrevious": " "
        },
    },
    dom:
        "<'pmd-card-title'<'data-table-responsive pull-left'><'search-paper pmd-textfield'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'pmd-card-footer' <'pmd-datatable-pagination' l i p>>",
});

$(document).ready(function() {
    exampleDatatable.init();
    $('.custom-select-info').hide();
    $(".custom-select-action").html('<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">delete</i></button><button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">more_vert</i></button>');
});

$('.sync-categories').on('click', function () {
    $.ajax({
        type: 'POST',
        url: '/admin/sun-line/sync/categories',
        data: {},
        success: function (data) {
            if (data.success === true) {
                location.reload();
            }
        },
        error: function (error) {
            console.log(error);
        },
    });
});

$('.sync-diasha-categories').on('click', function () {
    $.ajax({
        type: 'POST',
        url: '/admin/diasha/sync/categories',
        data: {},
        success: function (data) {
            if (data.success === true) {
                location.reload();
            }
        },
        error: function (error) {
            console.log(error);
        },
    });
});

$(".show_pagination").on("click", function () {
    let el = this;
    let nextEl = $(this).next();
    $(nextEl).show();
    $(el).hide();
});

$(".show_start_page_number").on("click", function () {
    let el = this;
    let nextEl = $(this).next();
    $(nextEl).show();
    $(el).hide();
});

$(".show_last_page_number").on("click", function () {
    let el = this;
    let nextEl = $(this).next();
    $(nextEl).show();
    $(el).hide();
});

$('.generate_report').on('click', function () {
    let tbodyEmptyClass = $("tbody#products-table tr td").first().attr('class');

    if (tbodyEmptyClass !== 'dataTables_empty') {
        $.ajax({
            type: 'POST',
            url: '/admin/sun-line/generate-products-report',
            data: {
                'category': $('tbody#products-table tr td.category-link a').first().text()
            },
            success: function (data) {
                if (data.success === true) {
                    location.href = '/admin/sun-line/products';
                }
            },
            error: function (error) {
                console.log(error);
            },
        });
    } else {
        $('.close').click();
    }
});

$('.generate_diasha_report').on('click', function () {
    let tbodyEmptyClass = $("tbody#products-table tr td").first().attr('class');

    if (tbodyEmptyClass !== 'dataTables_empty') {
        $.ajax({
            type: 'POST',
            url: '/admin/diasha/generate-products-report',
            data: {
                'category': $('tbody#products-table tr td.category-link a').first().text()
            },
            success: function (data) {
                if (data.success === true) {
                    location.href = '/admin/diasha/products';
                }
            },
            error: function (error) {
                console.log(error);
            },
        });
    } else {
        $('.close').click();
    }
});

$('.sync-some-products').on('click', function () {
    let list = [];
    let paginationArray = [];
    $('tr.selected').each(function () {
        list.push($(this).children('td').eq(1).text());
        paginationArray.push($(this).children('td').eq(4).children().eq(0).children().eq(0).text());
    });

    if (list.length === 0) {
        $.notify("Select category", {
            color: "#fff",
            background: "#ff5722",
            align:"right",
            verticalAlign:"top",
            animationType:"drop"
        });

        return
    }

    let paginationCheck = checkSomePagination(paginationArray);
    if (!paginationCheck) {
        $.notify("Fill pagination for category!", {
            color: "#fff",
            background: "#ff5722",
            align:"right",
            verticalAlign:"top",
            animationType:"drop"
        });
    } else {
        $('#center-dialog').modal('show');

        $.ajax({
            type: 'POST',
            url: '/admin/sun-line/sync/products',
            data: {
                'categories': list
            },
            success: function (data) {
                if (data.success === true) {
                    location.href = '/admin/sun-line/products';
                }
            },
            error: function (error) {
                console.log(error);
            },
        });
    }
});

$('.sync-diasha-some-products').on('click', function () {
    let list = [];
    let linkArray = [];
    $('tr.selected').each(function () {
        list.push($(this).children('td').eq(1).text());
        linkArray.push($(this).children('td').eq(3).children('a').eq(0).text());
    });

    if (list.length === 0) {
        $.notify("Select category", {
            color: "#fff",
            background: "#ff5722",
            align:"right",
            verticalAlign:"top",
            animationType:"drop"
        });

        return
    }

    let paginationCheck = checkSomePagination(linkArray);
    if (!paginationCheck) {
        $.notify("Fill link for category!", {
            color: "#fff",
            background: "#ff5722",
            align:"right",
            verticalAlign:"top",
            animationType:"drop"
        });
    } else {
        $('#center-dialog').modal('show');

        $.ajax({
            type: 'POST',
            url: '/admin/diasha/sync/products',
            data: {
                'categories': list
            },
            success: function (data) {
                if (data.success === true) {
                    location.href = '/admin/diasha/products';
                }
            },
            error: function (error) {
                console.log(error);
            },
        });
    }
});

$(".edit_pagination").click(function () {
    let id = $(this).data("id");
    let value = $(this).prev().val();
    let edit = $(this).parent();
    $.ajax(
        {
            url: '/admin/sun-line/categories/pagination/' + id,
            method: 'PUT',
            dataType: "JSON",
            data: {
                "pagination": value,
            },
            success: function (data) {
                if (data.success === true) {
                    let input = $(edit).prev();
                    $(input).show();
                    $(input).children('span').text(value);
                    $(edit).hide();
                }
            },
        }
    );
});

$(".edit_start_page_number").click(function () {
    let id = $(this).data("id");
    let startPageValue = $(this).prev().val();
    let edit = $(this).parent();
    let lastPageValue = $(this).parent().parent().next().children('div.show_last_page_number').children('span').text();

    if(startPageValue > lastPageValue && lastPageValue != '') {
		$.notify("The start page cannot be larger than the last one", {
			color: "#fff",
			background: "#ff5722",
			align:"right",
			verticalAlign:"top",
			animationType:"drop"
		});

		return
    }

    $.ajax(
        {
            url: '/admin/diasha/categories/' + id + '/page-numbers',
            method: 'PUT',
            dataType: "JSON",
            data: {
                "startPageValue": startPageValue,
                "lastPageValue": lastPageValue,
            },
            success: function (data) {
                if (data.success === true) {
                    $(edit).prev().show();
                    edit.prev().children('span').text(startPageValue);
                    $(edit).hide();
                }
            },
        }
    );
});

$(".edit_last_page_number").click(function () {
    let id = $(this).data("id");
    let lastPageValue = $(this).prev().val();
    let edit = $(this).parent();
	let startPageValue = $(this).parent().parent().prev().children('div.show_start_page_number').children('span').text();

	if(startPageValue > lastPageValue) {
		$.notify("The start page cannot be larger than the last one", {
			color: "#fff",
			background: "#ff5722",
			align:"right",
			verticalAlign:"top",
			animationType:"drop"
		});

		return
	}

    $.ajax(
        {
            url: '/admin/diasha/categories/' + id + '/page-numbers',
            method: 'PUT',
            dataType: "JSON",
            data: {
                "startPageValue": startPageValue,
                "lastPageValue": lastPageValue,
            },
            success: function (data) {
                if (data.success === true) {
                    $(edit).prev().show();
                    edit.prev().children('span').text(lastPageValue);
                    $(edit).hide();
                }
            },
        }
    );
});

$('.table').on('click', '.delete_product', function () {
    let id = $(this).data("id");
    let row = exampleDatatable.row($(this).parent().parent());
    let pageNumber = exampleDatatable.page.info().page;

    $.ajax(
        {
            url: '/admin/products/' + id,
            method: 'DELETE',
            dataType: "JSON",
            data: {
                "id": id,
            },
            success: function (data) {
                if (data.success === true) {
                    row.remove().draw();
                    exampleDatatable.page(pageNumber).draw('page');
                }
            },
        }
    );
});

$(".delete_file").click(function () {
    let id = $(this).data("id");
    if (confirm("Are you sure?")) {
        $.ajax(
            {
                url: '/admin/files/' + id,
                method: 'DELETE',
                dataType: "JSON",
                data: {
                    "id": id,
                },
                success: function (data) {
                    if (data.success === true) {
                        location.reload();
                    }
                },
            }
        );
    }
});

function checkSomePagination(paginationArray)
{
    let returnValue = null;
    $.each(paginationArray,function (index, value) {
        if (!value) {
            returnValue = value;
            return false
        } else {
            returnValue = value;
            return true;
        }
    });

    return returnValue;
}
