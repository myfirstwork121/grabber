<?php declare(strict_types=1);

/**
 * @author: Max Marchenko <max@heartpace.com>
 * @version:
 * Datetime: 29.10.2019 23:50
 */

namespace App\Controller;

use Doctrine\ORM\ORMException;
use App\Manager\SettingManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class SettingController
 * @package App\Controller
 */
class SettingController extends AbstractController
{
    private const EMAIL_SEND = 'emailSend';

    /**
     * @param Request $request
     * @param SettingManager $settingManager
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(Request $request, SettingManager $settingManager): JsonResponse
    {
        $emailSend = $this->stingToBoolean($request->get(self::EMAIL_SEND));
        $settingManager->update($emailSend);

        return $this->sendResponse(Response::HTTP_OK);
    }

    /**
     * @param SettingManager $settingManager
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function emailSendConfiguration(SettingManager $settingManager): JsonResponse
    {
        return $this->sendResponseWithData(
            [$settingManager->getEmailSendConfiguration()]
        );
    }
}
