<?php declare(strict_types=1);

/**
 * @author: Max Marchenko <max@heartpace.com>
 * @version: 1
 * Datetime: 03.11.2019 14:59
 */

namespace App\DataFixtures;

use App\Entity\Setting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class SettingFixtures
 * @package App\DataFixtures
 */
class SettingFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $setting = new Setting();
        $setting->setEmailSend(false);

        $manager->persist($setting);
        $manager->flush();
    }
}
