<?php declare(strict_types=1);

namespace App\DataMapper;

use App\DataTransferObject\DTOInterface\ProductParsedDataDTOInterface;
use App\Factory\ProductParsedDataFactory;

/**
 * Class ProductInfoDataMapper
 * @package App\DataMapper
 */
class ProductInfoDataMapper implements ProductInfoDataMapperInterface
{
    public const COLUMN_VENDOR_CODE = 'Код_товара';
    public const COLUMN_POSITION_NAME = 'Название_позиции';
    public const COLUMN_DESCRIPTION = 'Описание';
    public const COLUMN_META_KEYS = 'Ключевые_слова';
    public const COLUMN_PRODUCT_TYPE = 'Тип_товара';
    public const COLUMN_PRICE = 'Цена';
    public const COLUMN_CURRENCY = 'Валюта';
    public const COLUMN_UNIT = 'Единица_измерения';
    public const COLUMN_ORDER_QUANTITY = 'Минимальный_объем_заказа';
    public const COLUMN_WHOLESALE_PRICE = 'Оптовая_цена';
    public const COLUMN_ORDER_WHOLESALE = 'Минимальный_заказ_опт';
    public const COLUMN_IMAGE_LINK = 'Ссылка_изображения';
    public const COLUMN_AVAILABILITY = 'Наличие';
    public const COLUMN_AMOUNT = 'Количество';
    public const COLUMN_GROUP_NUMBER = 'Номер_группы';
    public const COLUMN_GROUP_NAME = 'Название_группы';
    public const COLUMN_SUBSECTION_ADDRESS = 'Адрес_подраздела';
    public const COLUMN_POSSIBILITY_OF_DELIVERY = 'Возможность_поставки';
    public const COLUMN_DELIVERY_TIME = 'Срок_поставки';
    public const COLUMN_PACKING_METHOD = 'Способ_упоковки';
    public const COLUMN_UNIQUE_ID = 'Уникальный_идентификатор';
    public const COLUMN_PRODUCT_ID = 'Идентификатор_товара ';
    public const COLUMN_SUB_SECTION_ID = 'Идентификатор_подраздела';
    public const COLUMN_GROUP_ID = 'Идентификатор_группы';
    public const COLUMN_VENDOR = 'Производитель';
    public const COLUMN_COUNTRY_OF_VENDOR = 'Страна_производитель';
    public const COLUMN_DISCOUNT = 'Скидка';
    public const COLUMN_SPECIES_GROUP_ID = 'ID_группы_разновидностей';
    public const COLUMN_TAGS= 'Метки';
    public const COLUMN_DISCOUNT_VALID_FROM = 'Cрок действия скидки от';
    public const COLUMN_DISCOUNT_VALID_TO= 'Cрок действия скидки до';
    public const COLUMN_PRICE_FROM = 'Цена от';
    public const COLUMN_CHARACTERISTICS_NAME = 'Название_Характеристики';
    public const COLUMN_CHARACTERISTICS_CHANGES = 'Измерение_Характеристики';
    public const COLUMN_CHARACTERISTICS_VALUE = 'Значение_Характеристики';

    public const COLUMN_VENDOR_SUN_VALUE = 'Линия Солнца';
    public const COLUMN_VENDOR_DIASHA_VALUE = 'Diasha';

    protected const COLUMN_PRODUCT_TYPE_VALUE = 'r';
    protected const COLUMN_CURRENCY_VALUE = 'UAH';
    protected const COLUMN_UNIT_VALUE = 'шт.';
    protected const COLUMN_AVAILABILITY_VALUE = '+';
    protected const COLUMN_COUNTRY_OF_VENDOR_VALUE = 'Китай';

    protected const KEY_NAME = 'name';
    protected const KEY_LINK = 'link';


    /**
     * @var ProductParsedDataFactory $factory
     */
    private $factory;

    /**
     * ProductInfoDataMapper constructor.
     * @param ProductParsedDataFactory $factory
     */
    public function __construct(ProductParsedDataFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param string $vendor
     * @param string|null $vendorCode
     * @param string|null $description
     * @param string|null $keyWords
     * @param string|null $positionName
     * @param int|null $price
     * @param string|null $images
     * @param int|null $uniqueId
     * @return ProductParsedDataDTOInterface
     */
    public function setData(
        string $vendor,
        ?string $vendorCode,
        ?string $description,
        ?string $keyWords,
        ?string $positionName,
        ?int $price,
        ?string $images,
        ?int $uniqueId
    ): ProductParsedDataDTOInterface {
        return $this->factory->create()
            ->setVendor($vendor)
            ->setVendorCode($vendorCode)
            ->setDescription($description)
            ->setKeyWords($keyWords)
            ->setPositionName($positionName)
            ->setPrice($price)
            ->setImages($images)
            ->setUniqueId($uniqueId);
    }

    /**
     * @param ProductParsedDataDTOInterface $entity
     * @return array
     */
    public function toArray(ProductParsedDataDTOInterface $entity): array
    {
        return [
            self::COLUMN_VENDOR_CODE => $entity->getVendorCode(),
            self::COLUMN_POSITION_NAME => $entity->getPositionName(),
            self::COLUMN_META_KEYS => $entity->getKeyWords(),
            self::COLUMN_DESCRIPTION => $entity->getDescription(),
            self::COLUMN_PRODUCT_TYPE => self::COLUMN_PRODUCT_TYPE_VALUE,
            self::COLUMN_PRICE => $entity->getPrice(),
            self::COLUMN_CURRENCY => self::COLUMN_CURRENCY_VALUE,
            self::COLUMN_UNIT => self::COLUMN_UNIT_VALUE,
            self::COLUMN_ORDER_QUANTITY => null,
            self::COLUMN_WHOLESALE_PRICE => null,
            self::COLUMN_ORDER_WHOLESALE => null,
            self::COLUMN_IMAGE_LINK => $entity->getImages(),
            self::COLUMN_AVAILABILITY => self::COLUMN_AVAILABILITY_VALUE,
            self::COLUMN_AMOUNT => null,
            self::COLUMN_GROUP_NUMBER => null,
            self::COLUMN_GROUP_NAME => null,
            self::COLUMN_SUBSECTION_ADDRESS => null,
            self::COLUMN_POSSIBILITY_OF_DELIVERY => null,
            self::COLUMN_DELIVERY_TIME => null,
            self::COLUMN_PACKING_METHOD => null,
            self::COLUMN_UNIQUE_ID => $entity->getUniqueId(),
            self::COLUMN_PRODUCT_ID => null,
            self::COLUMN_SUB_SECTION_ID => null,
            self::COLUMN_GROUP_ID => null,
            self::COLUMN_VENDOR => $entity->getVendor(),
            self::COLUMN_COUNTRY_OF_VENDOR => self::COLUMN_COUNTRY_OF_VENDOR_VALUE,
            self::COLUMN_DISCOUNT => null,
            self::COLUMN_SPECIES_GROUP_ID => null,
            self::COLUMN_TAGS => null,
            self::COLUMN_DISCOUNT_VALID_FROM => null,
            self::COLUMN_DISCOUNT_VALID_TO => null,
            self::COLUMN_PRICE_FROM => null,
            self::COLUMN_CHARACTERISTICS_NAME => null,
            self::COLUMN_CHARACTERISTICS_CHANGES => null,
            self::COLUMN_CHARACTERISTICS_VALUE => null,
        ];
    }
}
