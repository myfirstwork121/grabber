<?php declare(strict_types=1);

namespace App\DataTransferObject\DTOInterface;

/**
 * RepositoryInterface CategoryDTOInterface
 * @package App\DataTransferObject
 */
interface CategoryDTOInterface extends DataTransferObjectInterface
{
    /**
     * @return string
     */
    public function getSiteUrl(): string;

    /**
     * @return int
     */
    public function getPagination(): ?int;

    /**
     * @return string
     */
    public function getLinkPattern(): ?string;
}
