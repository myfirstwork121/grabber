<?php declare(strict_types=1);

namespace App\DataTransferObject\DTOInterface;

use App\Entity\EntityInterface;

/**
 * Interface ProductDTOInterface
 * @package App\DataTransferObject
 */
interface ProductDTOInterface extends DataTransferObjectInterface
{
    /**
     * @return string
     */
    public function getImage(): string;

    /**
     * @return EntityInterface
     */
    public function getCategory(): EntityInterface;
}
