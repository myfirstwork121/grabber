<?php declare(strict_types=1);

namespace App\DataTransferObject;

use App\DataTransferObject\DTOInterface\ProductDTOInterface;
use App\Entity\EntityInterface;

/**
 * Class ProductDTO
 * @package App\DataTransferObject
 */
class ProductDTO implements ProductDTOInterface
{
    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $link
     */
    private $link;

    /**
     * @var string $image
     */
    private $image;

    /**
     * @var EntityInterface $category
     */
    private $category;

    /**
     * ProductDTO constructor.
     * @param string $name
     * @param string $link
     * @param string $image
     * @param EntityInterface $category
     */
    public function __construct(
        string $name,
        string $link,
        string $image,
        EntityInterface $category
    ) {
        $this->name = $name;
        $this->link = $link;
        $this->image = $image;
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return EntityInterface
     */
    public function getCategory(): EntityInterface
    {
        return $this->category;
    }
}
