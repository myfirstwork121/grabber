<?php declare(strict_types=1);

namespace App\DataTransferObject;

use App\DataTransferObject\DTOInterface\ProductParsedDataDTOInterface;

/**
 * Class ProductParsedDataDTO
 */
class ProductParsedDataDTO implements ProductParsedDataDTOInterface
{
    /**
     * @var string $vendor
     */
    private $vendor;

    /**
     * @var string $vendorCode
     */
    private $vendorCode;

    /**
     * @var string $description
     */
    private $description;

    /**
     * @var string $keyWords
     */
    private $keyWords;

    /**
     * @var string $positionName
     */
    private $positionName;

    /**
     * @var integer $price
     */
    private $price;

    /**
     * @var string $images
     */
    private $images;

    /**
     * @var int $uniqueId
     */
    private $uniqueId;

    /**
     * @return string
     */
    public function getVendor(): string
    {
        return $this->vendor;
    }

    /**
     * @param string $vendor
     * @return ProductParsedDataDTOInterface
     */
    public function setVendor(string $vendor): ProductParsedDataDTOInterface
    {
        $this->vendor = $vendor;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVendorCode(): ?string
    {
        return $this->vendorCode;
    }

    /**
     * @param null|string $vendorCode
     * @return ProductParsedDataDTO
     */
    public function setVendorCode(?string $vendorCode): ProductParsedDataDTOInterface
    {
        $this->vendorCode = $vendorCode;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return ProductParsedDataDTO
     */
    public function setDescription(?string $description): ProductParsedDataDTOInterface
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getKeyWords(): ?string
    {
        return $this->keyWords;
    }

    /**
     * @param null|string $keyWords
     * @return ProductParsedDataDTO
     */
    public function setKeyWords(?string $keyWords): ProductParsedDataDTOInterface
    {
        $this->keyWords = $keyWords;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPositionName(): ?string
    {
        return $this->positionName;
    }

    /**
     * @param null|string $positionName
     * @return ProductParsedDataDTO
     */
    public function setPositionName(?string $positionName): ProductParsedDataDTOInterface
    {
        $this->positionName = $positionName;

        return $this;
    }

    /**
     * @return null|int
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param null|int $price
     * @return ProductParsedDataDTO
     */
    public function setPrice(?int $price): ProductParsedDataDTOInterface
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getImages(): ?string
    {
        return $this->images;
    }

    /**
     * @param null|string $images
     * @return ProductParsedDataDTO
     */
    public function setImages(string $images): ProductParsedDataDTOInterface
    {
        $this->images = $images;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUniqueId(): ?int
    {
        return $this->uniqueId;
    }

    /**
     * @param null|int $uniqueId
     * @return ProductParsedDataDTO
     */
    public function setUniqueId(?int $uniqueId): ProductParsedDataDTOInterface
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }
}
