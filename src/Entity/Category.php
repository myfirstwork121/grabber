<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @Table(name="categories")
 */
class Category implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pagination;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="File", mappedBy="category")
     */
    private $files;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siteName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkPattern;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $startPageNumber;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $lastPageNumber;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getPagination(): ?int
    {
        return $this->pagination;
    }

    public function setPagination(?int $pagination): self
    {
        $this->pagination = $pagination;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $sunLineProduct): self
    {
        if (!$this->products->contains($sunLineProduct)) {
            $this->products[] = $sunLineProduct;
            $sunLineProduct->setCategory($this);
        }

        return $this;
    }

    public function removeProduct(Product $sunLineProduct): self
    {
        if ($this->products->contains($sunLineProduct)) {
            $this->products->removeElement($sunLineProduct);
            // set the owning side to null (unless already changed)
            if ($sunLineProduct->getCategory() === $this) {
                $sunLineProduct->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setCategory($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
            // set the owning side to null (unless already changed)
            if ($file->getCategory() === $this) {
                $file->setCategory(null);
            }
        }

        return $this;
    }

    public function getSiteName(): ?string
    {
        return $this->siteName;
    }

    public function setSiteName(?string $siteName): self
    {
        $this->siteName = $siteName;

        return $this;
    }

    public function getLinkPattern(): ?string
    {
        return $this->linkPattern;
    }

    public function setLinkPattern(?string $linkPattern): self
    {
        $this->linkPattern = $linkPattern;

        return $this;
    }

    public function getStartPageNumber(): ?int
    {
        return $this->startPageNumber;
    }

    public function setStartPageNumber(?int $startPageNumber): self
    {
        $this->startPageNumber = $startPageNumber;

        return $this;
    }

    public function getLastPageNumber(): ?int
    {
        return $this->lastPageNumber;
    }

    public function setLastPageNumber(?int $lastPageNumber): self
    {
        $this->lastPageNumber = $lastPageNumber;

        return $this;
    }
}
