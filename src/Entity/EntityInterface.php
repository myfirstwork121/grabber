<?php declare(strict_types=1);

namespace App\Entity;

/**
 * RepositoryInterface EntityInterface
 * @package App\Entity
 */
interface EntityInterface
{
    public function getId(): ?int;
}
