<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SettingRepository")
 * @Table(name="settings")
 */
class Setting
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $emailSend;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmailSend(): ?bool
    {
        return $this->emailSend;
    }

    public function setEmailSend(bool $emailSend): self
    {
        $this->emailSend = $emailSend;

        return $this;
    }
}
