<?php declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Category;
use Doctrine\ORM\Event\PreFlushEventArgs;

/**
 * Class CategoryListeners
 * @package App\EventListener
 */
class CategoryListeners
{
    /**
     * @param Category $entity
     * @param PreFlushEventArgs $event
     */
    public function preFlush(Category $entity, PreFlushEventArgs $event): void
    {
        if (null === $entity->getId()) {
            $event->getEntityManager()->getRepository(Category::class)
                ->deleteAllBySite($entity->getSiteName());
        }
    }
}
