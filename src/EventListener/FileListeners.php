<?php declare(strict_types=1);

namespace App\EventListener;

use App\Entity\File;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FileListeners
 * @package App\EventListener
 */
class FileListeners
{
    /**
     * @var Filesystem $fileSystem
     */
    private $fileSystem;

    /**
     * FileListeners constructor.
     * @param Filesystem $fileSystem
     */
    public function __construct(Filesystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
    }

    /**
     * @param File $file
     */
    public function preRemove(File $file): void
    {
        $this->fileSystem->remove($file->getFilePath());
    }
}
