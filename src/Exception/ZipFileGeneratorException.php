<?php declare(strict_types=1);

namespace App\Exception;

use Exception;

/**
 * Class ZipFileGeneratorException
 * @package App\Exception
 */
class ZipFileGeneratorException extends Exception
{

}