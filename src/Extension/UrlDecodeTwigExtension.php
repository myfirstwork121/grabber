<?php declare(strict_types=1);

namespace App\Extension;

use App\Factory\TwigSimpleFilterFactory;
use Twig_Extension;
use Twig_Filter;

/**
 * Class UrlDecodeTwigExtension
 * @package App\Extension
 */
class UrlDecodeTwigExtension extends Twig_Extension
{
    private const URL_DECODE_FUNC = 'urlDecode';

    /**
     * @var TwigSimpleFilterFactory $factory
     */
    private $factory;

    /**
     * UrlDecodeTwigExtension constructor.
     * @param TwigSimpleFilterFactory $factory
     */
    public function __construct(TwigSimpleFilterFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return Twig_Filter[]
     */
    public function getFilters(): array
    {
        return [
            $this->factory->create(self::URL_DECODE_FUNC, static function (?string $string): ?string {
                return $string !== null ? urldecode($string): null;
            })
        ];
    }
}
