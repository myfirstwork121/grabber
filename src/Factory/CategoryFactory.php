<?php declare(strict_types=1);

namespace App\Factory;

use App\Entity\Category;

/**
 * Class CategoryFactory
 * @package App\Factory
 */
class CategoryFactory
{
    /**
     * @return Category
     */
    public function create(): Category
    {
        return new Category();
    }
}
