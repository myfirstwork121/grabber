<?php declare(strict_types=1);

namespace App\Factory;

use App\Entity\File;

/**
 * Class FileFactory
 * @package App\Factory
 */
class FileFactory
{
    /**
     * @return File
     */
    public function create(): File
    {
        return new File();
    }
}
