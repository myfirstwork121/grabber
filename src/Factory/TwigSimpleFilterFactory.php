<?php declare(strict_types=1);

namespace App\Factory;

use Twig_SimpleFilter;

/**
 * Class TwigSimpleFilterFactory
 * @package App\Factory
 */
class TwigSimpleFilterFactory
{
    /**
     * @param string $name
     * @param callable $callable $callable
     * @return Twig_SimpleFilter
     */
    public function create(string $name, callable $callable): Twig_SimpleFilter
    {
        return new Twig_SimpleFilter($name, $callable);
    }
}
