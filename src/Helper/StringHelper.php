<?php declare(strict_types=1);

namespace App\Helper;

use ArrayHelpers\Arr;
use Exception;

/**
 * Class StringHelper
 */
class StringHelper
{
    private const PRICE_NUMBER_PATTERN = '/\D/';
    private const EMPTY_VALUE = ' ';

    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param int $length
     * @return string
     * @throws Exception
     */
    public static function random($length = 16): string
    {
        $string = '';

        while (($len = strlen($string)) < $length) {
            $size = $length - $len;

            $bytes = random_bytes($size);

            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }

    /**
     * @param string $string
     * @return int
     */
    public static function getNumber(string $string): int
    {
        return (int)preg_replace(self::PRICE_NUMBER_PATTERN, self::EMPTY_VALUE, $string);
    }

    public static function getDiashaCategoryName(string $link)
    {
        $explodedUrl = explode('/', $link);
        return Arr::get($explodedUrl, 3);
    }

    /**
     * @param string $string
     * @return string
     */
    public static function toEnglishWords(string $string): string
    {
        $alf = [
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        ];
        $title = strtr($string, $alf);
        $title = mb_strtolower($title);
        $title = mb_ereg_replace('[^-0-9a-z]', '-', $title);
        $title = mb_ereg_replace('[-]+', '-', $title);

        return trim($title, '-');
    }

    /**
     * @param string $haystack
     * @param array $needles
     * @return bool|mixed
     */
    public static function strposArray(string $haystack, array $needles = [])
    {
        $chr = array();
        foreach ($needles as $needle) {
            $res = strpos($haystack, $needle);

            if ($res !== false) {
                $chr[$needle] = $res;
            }
        }

        if (empty($chr)) {
            return false;
        }

        return min($chr);
    }
}
