<?php declare(strict_types=1);

namespace App\Manager;

use App\DataTransferObject\DTOInterface\CategoryDTOInterface;
use App\Entity\Category;
use App\Entity\EntityInterface;
use App\Factory\CategoryFactory;
use App\Helper\StringHelper;
use App\Repository\RepositoryInterface\CategoryRepositoryInterface;
use App\Repository\RepositoryInterface\RepositoryInterface;
use Generator;

/**
 * Class CategoryManager
 * @package App\Manager
 */
class CategoryManager
{
    /**
     * @var CategoryFactory $factory
     */
    private $factory;

    /**
     * @var CategoryRepositoryInterface $repository
     */
    private $repository;

    /**
     * CategoryDataMapper constructor.
     * @param CategoryFactory $factory
     * @param CategoryRepositoryInterface $repository
     */
    public function __construct(
        CategoryFactory $factory,
        CategoryRepositoryInterface $repository
    ) {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    /**
     * @param string $site
     * @return RepositoryInterface[]
     */
    public function getAllBySite(string $site): array
    {
        return $this->repository->findBy([RepositoryInterface::COLUMN_SITE => $site]);
    }

    /**
     * @param Generator $categories
     * @return void
     */
    public function create(Generator $categories): void
    {
        $categories = $this->prepare($categories);
        $this->repository->save($categories);
    }

    /**
     * @param array $ids
     * @return EntityInterface[]
     */
    public function getByIds(array $ids): array
    {
        return $this->repository->findByIds($ids);
    }

    /**
     * @param string $category
     * @return EntityInterface
     */
    public function getByName(string $category): EntityInterface
    {
        return $this->repository->findByName($category);
    }

    /**
     * @param int $pagination
     * @param int $id
     */
    public function refreshPagination(int $pagination, int $id): void
    {
        $this->repository->updatePagination($pagination, $id);
    }

    /**
     * @param int $startPage
     * @param int $lastPage
     * @param int $categoryId
     */
    public function refreshPageNumbers(int $startPage, int $lastPage, int $categoryId): void
    {
        $this->repository->updatePageNumbers($startPage, $lastPage, $categoryId);
    }

    /**
     * @param Generator $categories
     * @return Generator|null
     */
    private function prepare(Generator $categories): ?Generator
    {
        foreach ($categories as $category) {
            yield $this->fillData($category);
        }
    }

    /**
     * @param CategoryDTOInterface $categoryDTO
     * @return Category
     */
    private function fillData(CategoryDTOInterface $categoryDTO): EntityInterface
    {
        return $this->factory->create()
            ->setName(StringHelper::toEnglishWords($categoryDTO->getName()))
            ->setLink($categoryDTO->getLink())
            ->setSiteName($categoryDTO->getSiteUrl())
            ->setPagination($categoryDTO->getPagination())
            ->setLinkPattern($categoryDTO->getLinkPattern());
    }
}
