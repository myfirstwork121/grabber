<?php declare(strict_types=1);

namespace App\Manager;

use App\Entity\EntityInterface;
use App\Entity\File;
use App\Factory\FileFactory;
use App\Repository\FileRepository;
use App\Repository\RepositoryInterface\FileRepositoryInterface;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Generator;

/**
 * Class FileManager
 * @package App\Manager
 */
class FileManager
{
    /**
     * @var FileRepository $repository
     */
    private $repository;

    /**
     * @var FileFactory $factory
     */
    private $factory;

    /**
     * FileService constructor.
     * @param FileRepositoryInterface $repository
     * @param FileFactory $factory
     */
    public function __construct(FileRepositoryInterface $repository, FileFactory $factory)
    {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->repository->findAll();
    }

    /**
     * @param EntityInterface $category
     * @param string $filePath
     * @param string $fileName
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function create(EntityInterface $category, string $filePath, string $fileName): void
    {
        $entity = $this->prepare($category, $filePath, $fileName);
        $this->repository->save($entity);
    }

    /**
     * @param int $id
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(int $id): void
    {
        $this->repository->delete($id);
    }

    /**
     * @param EntityInterface $category
     * @param string $filePath
     * @param string $fileName
     * @return Generator
     * @throws Exception
     */
    private function prepare(EntityInterface $category, string $filePath, string $fileName): Generator
    {
        $entity = $this->fillData($filePath, $fileName, $category);
        yield from [$entity];
    }

    /**
     * @param string $pathToFile
     * @param string $fileName
     * @param EntityInterface $category
     * @return File
     * @throws Exception
     */
    private function fillData(string $pathToFile, string $fileName, EntityInterface $category): File
    {
        return $this->factory->create()->setFilePath($pathToFile)->setFileName($fileName)->setCategory($category)
            ->setDate(new DateTime('now'));
    }
}
