<?php declare(strict_types=1);

namespace App\Manager;

use App\Factory\ProductFactory;
use App\Helper\DomainHelper;
use App\Repository\RepositoryInterface\ProductRepositoryInterface;
use App\Repository\RepositoryInterface\RepositoryInterface;
use Generator;

/**
 * Class ProductManager
 * @package App\Manager
 */
class ProductManager
{
    /**
     * @var ProductRepositoryInterface $repository
     */
    private $repository;

    /**
     * @var ProductFactory $factory
     */
    private $factory;

    /**
     * ProductManager constructor.
     * @param ProductRepositoryInterface $repository
     * @param ProductFactory $factory
     */
    public function __construct(
        ProductRepositoryInterface $repository,
        ProductFactory $factory
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @param string $site
     * @return ProductRepositoryInterface[]
     */
    public function getAllBySite(string $site): array
    {
        return $this->repository->findBy([RepositoryInterface::COLUMN_SITE => $site]);
    }

    /**
     * @param Generator $products
     */
    public function create(Generator $products): void
    {
        $this->repository->save($this->fillData($products));
    }

    /**
     * @param int $id
     */
    public function remove(int $id): void
    {
        $this->repository->delete($id);
    }

    /**
     * @return void
     */
    public function removeAll(): void
    {
        $this->repository->deleteAll();
    }

    /**
     * @param Generator $products
     * @return Generator
     */
    private function fillData(Generator $products): Generator
    {
        foreach ($products as $product) {
            yield $this->factory->create()
                ->setName($product->getName())
                ->setLink($product->getLink())
                ->setImage($product->getImage())
                ->setCategory($product->getCategory())
                ->setSiteName(DomainHelper::getBaseUrl($product->getLink()));
        }
    }
}
