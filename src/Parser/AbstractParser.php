<?php declare(strict_types=1);

namespace App\Parser;

use App\Entity\Category;
use App\Entity\EntityInterface;
use Generator;
use PHPHtmlParser\Dom;
use GuzzleHttp\Client;

/**
 * Class AbstractParsingRepository
 */
abstract class AbstractParser
{
    protected const HREF_ATTRIBUTE = 'href';
    protected const SRC_ATTRIBUTE = 'src';
    protected const TITLE_ATTRIBUTE = 'title';
    protected const ALT_ATTRIBUTE = 'alt';
    protected const H2 = 'h2';
    protected const LINK = 'a';
    protected const IMG = 'img';
    protected const CONTENT_ATTRIBUTE = 'content';
    protected const HTML_LINE_BREAK = '<br />';

    protected const LINE_BREAK = "\n";
    protected const SEMICOLON = ';';
    protected const DELIMITER = ', ';
    protected const DESCRIPTION_GLUE = '; ';

    protected const FIRST_ELEMENT = 0;
    protected const EMPTY_VALUE = ' ';

    /**
     * @var Dom $domParser
     */
    protected $domParser;

    /**
     * @var Client $httpClient
     */
    protected $httpClient;

    /**
     * AbstractParsingRepository constructor.
     * @param Dom $dom
     * @param Client $httpClient
     */
    public function __construct(Dom $dom, Client $httpClient)
    {
        $this->domParser = $dom;
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $link
     * @return string
     */
    protected function getHtml(string $link): string
    {
        $response = $this->httpClient->get($link);
        return $response->getBody()->getContents();
    }

    /**
     * @param EntityInterface[] $categories
     * @return Generator
     */
    public function parseAllByCategories(array $categories): Generator
    {
        foreach ($categories as $category) {
            yield from $this->parse($category);
        }
    }

    /**
     * @param Category|EntityInterface $category
     * @return array
     */
    protected function parse(Category $category): array
    {
        return [$category];
    }
}
