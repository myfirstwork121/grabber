<?php declare(strict_types=1);

namespace App\Parser;

use App\DataMapper\ProductInfoDataMapper;
use App\DataMapper\ProductInfoDataMapperInterface;
use App\Entity\Category;
use App\Entity\ProductInterface;
use App\Factory\ProductDTOFactory;
use App\Helper\StringHelper;
use Generator;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\Collection;
use PHPHtmlParser\Dom\HtmlNode;

/**
 * Class SunProductParser
 * @package App\ParsingRepository
 */
class SunProductParser extends AbstractParser implements ProductParserInterface
{
    private const PAGINATION_LINK_PATTERN = '%s?page=%s';
    private const PRODUCT_HTML_PATTERN = 'div.image a';

    private const MAIN_CONTENT_HTML_PATTERN = 'div#content %s';
    private const VENDOR_CODE_HTML_PATTERN = 'h1';
    private const KEY_WORLDS_HTML_PATTERN = 'head meta';
    private const PRODUCT_INFORMATION_HTML_PATTERN = 'div.product-info div.right p';
    private const PRICE_HTML_PATTERN = 'div.product-info div.right div.description';
    private const IMAGES_HTML_PATTERN = 'div.product-info div.image-additional a';
    private const ONE_IMAGE_HTML_PATTERN = 'div.product-info div.left div.image a';

    /**
     * @var ProductDTOFactory $factory
     */
    private $factory;

    /**
     * @var ProductInfoDataMapper $productDataMapper
     */
    private $productDataMapper;

    /**
     * SunProductParser constructor.
     * @param Dom $dom
     * @param Client $httpClient
     * @param ProductDTOFactory $factory
     * @param ProductInfoDataMapperInterface $productDataMapper
     */
    public function __construct(
        Dom $dom,
        Client $httpClient,
        ProductDTOFactory $factory,
        ProductInfoDataMapperInterface $productDataMapper
    ) {
        parent::__construct($dom, $httpClient);
        $this->factory = $factory;
        $this->productDataMapper = $productDataMapper;
    }

    /**
     * @param ProductInterface $product
     * @return Generator
     */
    public function findInnerData(ProductInterface $product): Generator
    {
        $document = $this->getHtml($product->getLink());

        $this->domParser->load($document);
        $mainData = $this->findMainData($this->domParser);

        yield from [$this->productDataMapper->setData(
            ProductInfoDataMapper::COLUMN_VENDOR_SUN_VALUE,
            $this->findVendorCode($this->domParser),
            implode(self::DESCRIPTION_GLUE, $mainData),
            $this->findKeyWords($this->domParser),
            $this->findPositionName($mainData),
            $this->findPrice($this->domParser),
            $this->findImages($this->domParser),
            mt_rand()
        )];
    }

    /**
     * @param Dom $dom
     * @return string|null
     */
    private function findVendorCode(Dom $dom): ?string
    {
        /** @var Collection $vendorCodeCollection */
        $vendorCodeCollection = $dom->find(
            sprintf(self::MAIN_CONTENT_HTML_PATTERN, self::VENDOR_CODE_HTML_PATTERN)
        );

        if ($vendorCodeCollection !== null) {
            return $vendorCodeCollection->offsetGet(self::FIRST_ELEMENT)->text();
        }

        return null;
    }

    /**
     * @param Dom $dom
     * @return array
     */
    private function findMainData(Dom $dom): array
    {
        /** @var Collection $mainData */
        $mainData = $dom->find(
            sprintf(self::MAIN_CONTENT_HTML_PATTERN, self::PRODUCT_INFORMATION_HTML_PATTERN)
        );

        $mainDataArray = [];
        foreach ($mainData as $data) {
            if ($data->text() !== self::EMPTY_VALUE) {
                $mainDataArray[] = $data->text();
            } else {
                $mainDataArray[] = $this->findSquare($mainData);
            }
        }

        return $mainDataArray;
    }

    /**
     * @param Collection $collection
     * @return string|null
     */
    private function findSquare(Collection $collection): ?string
    {
        $lastElementNumber = $collection->count() -1;
        $lastElement = $collection->offsetGet($lastElementNumber);

        /** @var HtmlNode $lastElement */
        $children = $lastElement->getChildren();
        if (!empty($children)) {
            return end($children)->text();
        }

        return null;
    }

    /**
     * @param Dom $dom
     * @return string|null
     */
    private function findKeyWords(Dom $dom): ?string
    {
        /** @var Collection $keyWords */
        $keyWords = $dom->find(self::KEY_WORLDS_HTML_PATTERN);

        if ($keyWords !== null) {
            return $keyWords->offsetGet(self::FIRST_ELEMENT)
                ->getAttribute(self::CONTENT_ATTRIBUTE);
        }

        return null;
    }

    /**
     * @param array $mainData
     * @return string|null
     */
    private function findPositionName(array $mainData): ?string
    {
        return array_shift($mainData);
    }

    /**
     * @param Dom $dom
     * @return int|null
     */
    private function findPrice(Dom $dom): ?int
    {
        /** @var Collection $priceCollection */
        $priceCollection = $dom->find(
            sprintf(self::MAIN_CONTENT_HTML_PATTERN, self::PRICE_HTML_PATTERN)
        );

        $firstElement = $priceCollection->offsetGet(self::FIRST_ELEMENT);
        if ($firstElement !== null) {
            $priceLine = $firstElement !== null ? $firstElement->text(): self::EMPTY_VALUE;
            return StringHelper::getNumber($priceLine);
        }

        return null;
    }

    /**
     * @param Dom $dom
     * @return string
     */
    private function findImages(Dom $dom): string
    {
        /** @var Collection $imagesCollection */
        $imagesCollection = $dom->find(
            sprintf(self::MAIN_CONTENT_HTML_PATTERN, self::IMAGES_HTML_PATTERN)
        );

        $firstElement = $imagesCollection->offsetGet(self::FIRST_ELEMENT);

        $images = [];
        if ($firstElement !== null) {
            foreach ($imagesCollection as $value) {
                /** @var HtmlNode $value */
                $images[] = $value->getAttribute(self::HREF_ATTRIBUTE);
            }
        }

        /** @var Collection $imageElement */
        $imageElement = $dom->find(
            sprintf(self::MAIN_CONTENT_HTML_PATTERN, self::ONE_IMAGE_HTML_PATTERN)
        );

        $images[] = $imageElement->offsetGet(self::FIRST_ELEMENT) ?
            $imageElement->offsetGet(self::FIRST_ELEMENT)->getAttribute(self::HREF_ATTRIBUTE): null;

        $images = array_unique($images);

        return implode(self::DELIMITER, $images);
    }

    /**
     * @param Category $category
     * @return array
     */
    protected function parse(Category $category): array
    {
        $products = [];
        for ($i = 1; $i <= $category->getPagination(); $i++) {
            $document = $this->getHtml(sprintf(self::PAGINATION_LINK_PATTERN, $category->getLink(), $i));
            $this->domParser->load($document);

            foreach ($this->domParser->find(self::PRODUCT_HTML_PATTERN) as $value) {
                /** @var HtmlNode $value */
                $children = $value->getChildren();
                $image = next($children);

                $products[] = $this->factory->create(
                    $image->getAttribute(self::TITLE_ATTRIBUTE),
                    $value->getAttribute(self::HREF_ATTRIBUTE),
                    $image->getAttribute(self::SRC_ATTRIBUTE),
                    $category
                );
            }
        }

        return $products;
    }
}
