<?php declare(strict_types=1);

namespace App\Queues\QueuesInterface;

/**
 * Interface DiashaConsumerInterface
 * @package App\Queues\QueuesInterface
 */
interface DiashaConsumerInterface extends ConsumerInterface
{

}
