<?php declare(strict_types=1);

namespace App\Queues\QueuesInterface;

/**
 * interface SunConsumerInterface
 * @package App\Queues\QueuesInterface
 */
interface SunConsumerInterface extends ConsumerInterface
{

}
