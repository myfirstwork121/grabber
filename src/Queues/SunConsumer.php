<?php declare(strict_types=1);

namespace App\Queues;

use App\Exception\ZipFileGeneratorException;
use App\Manager\CategoryManager;
use App\Manager\FileManager;
use App\Parser\ProductParserInterface;
use App\Queues\QueuesInterface\SunConsumerInterface;
use App\Service\EmailService;
use App\Service\ProductService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Enqueue\Dbal\DbalConnectionFactory;
use Enqueue\Dbal\DbalDestination;
use PHPMailer\PHPMailer\Exception;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

/**
 * Class SunConsumer
 * @package App\Queues
 */
class SunConsumer implements SunConsumerInterface
{
    /**
     * @var DbalConnectionFactory $context
     */
    private $context;

    /**
     * @var DbalDestination $dbalDestination
     */
    private $dbalDestination;

    /**
     * @var ProductService $service
     */
    private $service;

    /**
     * @var EmailService $emailService
     */
    private $emailService;

    /**
     * @var FileManager $fileManager
     */
    private $fileManager;

    /**
     * @var CategoryManager $categoryManager
     */
    private $categoryManager;

    /**
     * @var ProductParserInterface $parser
     */
    private $parser;

    /**
     * SunConsumer constructor.
     * @param DbalConnectionFactory $context
     * @param DbalDestination $dbalDestination
     * @param ProductService $service
     * @param EmailService $emailService
     * @param FileManager $fileManager
     * @param CategoryManager $categoryManager
     * @param ProductParserInterface $parser
     */
    public function __construct(
        DbalConnectionFactory $context,
        DbalDestination $dbalDestination,
        ProductService $service,
        EmailService $emailService,
        FileManager $fileManager,
        CategoryManager $categoryManager,
        ProductParserInterface $parser
    ) {
        $this->context = $context->createContext();
        $this->dbalDestination = $dbalDestination;
        $this->service = $service;
        $this->emailService = $emailService;
        $this->fileManager = $fileManager;
        $this->categoryManager = $categoryManager;
        $this->parser = $parser;
    }

    /**
     * @param string $site
     * @throws ZipFileGeneratorException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function startProcessing(string $site): void
    {
        if ($this->service->getQueueCount(Producer::QUEUE_SUN_NAME) !== 0) {
            $consumer = $this->context->createConsumer($this->dbalDestination);
            $message = $consumer->receive();
            $consumer->acknowledge($message);

            $this->service->addParser($this->parser, $site);
            $this->service->setParser($site);
            $filePath = $this->service->generateFile();

            $category = $message->getBody();
            $this->emailService->send($filePath, EmailService::SUN_LINE_TEMPLATE_NAME, $category, $site);

            $category = $this->categoryManager->getByName($category);
            $this->fileManager->create($category, $filePath, basename($filePath));
        }
    }
}
