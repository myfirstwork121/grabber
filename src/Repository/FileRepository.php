<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\File;
use App\Repository\RepositoryInterface\FileRepositoryInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method File|null find($id, $lockMode = null, $lockVersion = null)
 * @method File|null findOneBy(array $criteria, array $orderBy = null)
 * @method File[]    findAll()
 * @method File[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileRepository extends AbstractRepository implements FileRepositoryInterface
{
    public const COLUMN_FILE_NAME = 'fileName';

    /**
     * FileRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, File::class);
    }

    /**
     * @param string $fileName
     * @return string|null
     */
    public function getFilePath(string $fileName): ?string
    {
        $file = $this->findOneBy([self::COLUMN_FILE_NAME => $fileName]);

        if ($file !== null) {
            return $file->getFilePath();
        }

        return null;
    }
}
