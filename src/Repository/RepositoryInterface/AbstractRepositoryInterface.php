<?php declare(strict_types=1);

namespace App\Repository\RepositoryInterface;

use Generator;

/**
 * Class AbstractRepositoryInterface
 * @package App\Repository\RepositoryInterface
 */
interface AbstractRepositoryInterface
{
    /**
     * @param Generator $entities
     * @return void
     */
    public function save(Generator $entities): void;

    /**
     * @return void
     */
    public function deleteAll(): void;

    /**
     * @param string $site
     */
    public function deleteAllBySite(string $site): void;

    /**
     * @param string $queueName
     * @return int
     */
    public function fetchQueueCount(string $queueName): int;

    /**
     * @param array $array
     * @return array
     */
    public function findByIds(array $array): array;

    /**
     * @param int $id
     */
    public function delete(int $id): void;
}
