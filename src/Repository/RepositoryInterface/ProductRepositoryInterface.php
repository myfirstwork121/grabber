<?php declare(strict_types=1);

namespace App\Repository\RepositoryInterface;

use Generator;

/**
 * Interface ProductRepositoryInterface
 * @package App\Repository\RepositoryInterface
 */
interface ProductRepositoryInterface extends RepositoryInterface
{
    /**
     * @param Generator $entities
     * @param int $limit
     */
    public function save(Generator $entities, int $limit = 1000): void;
}
