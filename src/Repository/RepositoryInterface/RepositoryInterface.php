<?php declare(strict_types=1);

namespace App\Repository\RepositoryInterface;

/**
 * interface RepositoryInterface
 * @package App\Repository\RepositoryInterface
 */
interface RepositoryInterface extends AbstractRepositoryInterface
{
    public const COLUMN_ID = 'id';
    public const COLUMN_SITE = 'siteName';

    /**
     * @param int $id
     * @param null $lockMode
     * @param null $lockVersion
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param array $criteria
     * @param array|null $orderBy
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    public function findAll();

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
}
