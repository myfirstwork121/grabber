<?php declare(strict_types=1);

namespace App\Service;

use App\DataMapper\ProductInfoDataMapper;
use App\Helper\KernelHelper;
use App\Helper\StringHelper;
use App\Parser\AbstractParser;
use App\Repository\AbstractRepository;
use App\Repository\RepositoryInterface\AbstractRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Exception;

/**
 * Class AbstractService
 * @package App\Service
 */
abstract class AbstractService
{
    protected const COLUMN_TYPE_STRING = 'string';
    protected const COLUMN_TYPE_PRICE = 'price';
    protected const COLUMN_TYPE_INTEGER = 'integer';

    public const COLUMN = [
        ProductInfoDataMapper::COLUMN_VENDOR_CODE                => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_POSITION_NAME              => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_META_KEYS                  => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_DESCRIPTION                => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_PRODUCT_TYPE               => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_PRICE                      => self::COLUMN_TYPE_PRICE,
        ProductInfoDataMapper::COLUMN_CURRENCY                   => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_UNIT                       => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_ORDER_QUANTITY             => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_WHOLESALE_PRICE            => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_ORDER_WHOLESALE            => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_IMAGE_LINK                 => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_AVAILABILITY               => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_AMOUNT                     => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_GROUP_NUMBER               => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_GROUP_NAME                 => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_SUBSECTION_ADDRESS         => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_POSSIBILITY_OF_DELIVERY    => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_DELIVERY_TIME              => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_PACKING_METHOD             => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_UNIQUE_ID                  => self::COLUMN_TYPE_INTEGER,
        ProductInfoDataMapper::COLUMN_PRODUCT_ID                 => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_SUB_SECTION_ID             => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_GROUP_ID                   => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_VENDOR                     => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_COUNTRY_OF_VENDOR          => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_DISCOUNT                   => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_SPECIES_GROUP_ID           => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_TAGS                       => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_DISCOUNT_VALID_FROM        => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_DISCOUNT_VALID_TO          => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_PRICE_FROM                 => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_CHARACTERISTICS_NAME       => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_CHARACTERISTICS_CHANGES    => self::COLUMN_TYPE_STRING,
        ProductInfoDataMapper::COLUMN_CHARACTERISTICS_VALUE      => self::COLUMN_TYPE_STRING,
    ];

    /**
     * @var ServiceEntityRepository $repository
     */
    protected $repository;

    /**
     * @var AbstractParser $parser
     */
    protected $parser;

    /**
     * @var ProductInfoDataMapper $dataMapper
     */
    protected $dataMapper;

    /**
     * @var AbstractRepository|null $categoryRepository
     */
    protected $categoryRepository;

    /**
     * AbstractService constructor.
     * @param AbstractRepositoryInterface $repository
     * @param AbstractParser|null $parsingRepository
     * @param ProductInfoDataMapper|null $dataMapper
     */
    public function __construct(
        AbstractRepositoryInterface $repository = null,
        AbstractParser $parsingRepository = null,
        ProductInfoDataMapper $dataMapper = null
    ) {
        $this->repository = $repository;
        $this->parser = $parsingRepository;
        $this->dataMapper = $dataMapper;
    }

    /**
     * @param string $queueName
     * @return int
     */
    public function getQueueCount(string $queueName): int
    {
        return $this->repository->fetchQueueCount($queueName);
    }

    /**
     * @param string $filePathPattern
     * @return string
     * @throws Exception
     */
    protected function generateFileName(string $filePathPattern): string
    {
        return sprintf($filePathPattern, KernelHelper::getPublicDir(), StringHelper::random(10));
    }
}
