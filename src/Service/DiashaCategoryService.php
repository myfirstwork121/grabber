<?php declare(strict_types=1);

namespace App\Service;

use App\Parser\DiashaCategoryParser as ParsingRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Generator;

/**
 * Class DiashaService
 * @package App\Service
 */
class DiashaCategoryService extends AbstractService
{
//    /**
//     * @var DiashaCategoryParser $repository
//     */
//    protected $repository;
//
//    /**
//     * @var ParsingRepository $parsingRepository
//     */
//    protected $parsingRepository;
//
//    /**
//     * @var DiashaCategoryDataMapper $dataMapper
//     */
//    protected $dataMapper;
//
//    /**
//     * DiashaService constructor.
//     * @param DiashaCategoryParser $repository
//     * @param ParsingRepository $parsingRepository
//     * @param DiashaCategoryDataMapper $dataMapper
//     */
//    public function __construct(
//        DiashaCategoryParser $repository,
//        ParsingRepository $parsingRepository,
//        DiashaCategoryDataMapper $dataMapper
//    ) {
//        parent::__construct($repository);
//        $this->parsingRepository = $parsingRepository;
//        $this->dataMapper = $dataMapper;
//    }
//
//    /**
//     * @throws ORMException
//     * @throws OptimisticLockException
//     */
//    public function synchronizeCategories(): void
//    {
//        $this->repository->deleteAllRecords();
//        $categories = $this->prepareCategories();
//        $this->repository->save($categories);
//    }
//
//
//    /**
//     * @return Generator
//     */
//    private function prepareCategories(): Generator
//    {
//        $categories = $this->parsingRepository->findCategories();
//        foreach ($categories as $categoryName) {
//            yield $this->dataMapper->setData($categoryName);
//        }
//    }
//
//    /**
//     * @param string $link
//     * @param int $categoryId
//     * @throws ORMException
//     * @throws OptimisticLockException
//     */
//    public function updateLink(string $link, int $categoryId): void
//    {
//        $this->repository->updateLink($link, $categoryId);
//    }
}
