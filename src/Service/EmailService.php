<?php declare(strict_types=1);

namespace App\Service;

use App\Helper\EnvVarHelper;
use App\Helper\StringHelper;
use App\Manager\SettingManager;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use PHPMailer\PHPMailer\Exception;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig_Environment;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Class EmailService
 * @package App\Service
 */
class EmailService extends AbstractService
{
    public const SUN_LINE_TEMPLATE_NAME = 'emails/report-template.html.twig';

    private const LINK_PARAM = 'link';
    private const SITE_PARAM = 'site';
    private const CATEGORY_PARAM = 'category';
    private const LINK_PATTERN = '%s/admin/files/%s';
    private const SUBJECT_PATTERN = '%s-%s';
    private const DATE_FORMAT = 'Y-m-d H:i:s';
    private const VERBOSE_DEBUG_OUTPUT = 2;
    private const CUSTOMER_EMAIL = 'lehanovskiiy@gmail.com';
    private const DEVELOPER_EMAIL = 'myfirstwork121@gmail.com';
    private const DEV_ENV = 'dev';

    private const TO_EMAILS = [self::CUSTOMER_EMAIL];

    private const ERROR_MESSAGE_PATTERN = 'Message could not be sent. Mailer Error: %s';

    private const PARAM_MAILER_HOST = 'MAILER_HOST';
    private const PARAM_MAILER_USERNAME = 'MAILER_USERNAME';
    private const PARAM_MAILER_PASS = 'MAILER_PASSWORD';
    private const PARAM_MAILER_SMTP_SECURE = 'MAILER_SMTP_SECURE';
    private const PARAM_MAILER_PORT = 'MAILER_PORT';
    private const PARAM_MAILER_FROM = 'MAILER_FROM';
    private const PARAM_APP_URL= 'APP_URL';

    /**
     * @var Twig_Environment $twigEnvironment
     */
    private $twigEnvironment;

    /**
     * @var SettingManager
     */
    private $settingsManager;

    /**
     * @var DateTime $dateTime
     */
    private $dateTime;

    /**
     * @var PHPMailer $mailer
     */
    private $mailer;

    /**
     * @var string $mailerFrom
     */
    private $mailerFrom;

    /**
     * @var string $domain
     */
    private $domain;

    /**
     * EmailHelper constructor.
     * @param PHPMailer $mailer
     * @param DateTime $dateTime
     * @param SettingManager $settingManager
     * @param Twig_Environment $twigEnvironment
     */
    public function __construct(
        PHPMailer $mailer,
        DateTime $dateTime,
        SettingManager $settingManager,
        Twig_Environment $twigEnvironment
    ) {
        parent::__construct();
        $this->twigEnvironment = $twigEnvironment;
        $this->settingsManager = $settingManager;
        $this->dateTime = $dateTime;
        $this->mailer = $mailer;
        $this->configureMailer();
    }

    /**
     * @param string $pathToFile
     * @param string $template
     * @param string $category
     * @param string $site
     * @param array $recipients
     * @throws Exception
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws NonUniqueResultException
     */
    public function send(
        string $pathToFile,
        string $template,
        string $category,
        string $site,
        array $recipients = self::TO_EMAILS
    ): void {
        if ($this->settingsManager->getEmailSendConfiguration()) {
            $this->prepareDataForSend($pathToFile, $template, $category, $site, $recipients);
        }
    }

    /**
     * @param string $pathToFile
     * @param string $template
     * @param string $category
     * @param string $site
     * @param array $recipients
     * @throws Exception
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function prepareDataForSend(
        string $pathToFile,
        string $template,
        string $category,
        string $site,
        array $recipients
    ): void {
        $link = $this->buildLink($pathToFile);
        $subject = $this->makeSubject($category);
        $body = $this->renderTemplate($template, $link, $category, $site);
        try {
            $this->mailer->setFrom($this->mailerFrom, $this->domain);
            foreach ($recipients as $recipient) {
                $this->mailer->addAddress($recipient);
            }

            if (EnvVarHelper::getEnv(EnvVarHelper::APP_ENV) === self::DEV_ENV) {
                $this->mailer->addAddress(self::DEVELOPER_EMAIL);
            }

            $this->mailer->isHTML();
            $this->mailer->Subject = $subject;
            $this->mailer->Body = $body;

            $this->mailer->send();
        } catch (Exception $e) {
            throw new Exception(sprintf(self::ERROR_MESSAGE_PATTERN, $this->mailer->ErrorInfo));
        }
    }

    /**
     * @param string $category
     * @return string
     */
    private function makeSubject(string $category): string
    {
        return sprintf(
            self::SUBJECT_PATTERN,
            StringHelper::toEnglishWords($category),
            $this->dateTime->format(self::DATE_FORMAT)
        );
    }

    /**
     * @param string $template
     * @param string|null $link
     * @param string|null $category
     * @param string $site
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function renderTemplate(
        string $template,
        string $link = null,
        string $category = null,
        string $site = null
    ): string {
        return $this->twigEnvironment->render(
            $template,
            [
                self::LINK_PARAM => $link,
                self::CATEGORY_PARAM => $category,
                self::SITE_PARAM => $site
            ]
        );
    }

    /**
     * @param string $rawLink
     * @return string
     */
    private function buildLink(string $rawLink): string
    {
        return sprintf(self::LINK_PATTERN, $this->domain, basename($rawLink));
    }

    /**
     * @return void
     */
    private function configureMailer(): void
    {
        $this->mailer->SMTPDebug    = self::VERBOSE_DEBUG_OUTPUT;
        $this->mailer->isSMTP();
        $this->mailer->Host         = EnvVarHelper::getEnv(self::PARAM_MAILER_HOST);
        $this->mailer->SMTPAuth     = true;
        $this->mailer->Username     = EnvVarHelper::getEnv(self::PARAM_MAILER_USERNAME);
        $this->mailer->Password     = EnvVarHelper::getEnv(self::PARAM_MAILER_PASS);
        $this->mailer->SMTPSecure   = EnvVarHelper::getEnv(self::PARAM_MAILER_SMTP_SECURE);
        $this->mailer->Port         = (int)EnvVarHelper::getEnv(self::PARAM_MAILER_PORT);
        $this->mailerFrom           = EnvVarHelper::getEnv(self::PARAM_MAILER_FROM);
        $this->domain               = EnvVarHelper::getEnv(self::PARAM_APP_URL);
    }
}
