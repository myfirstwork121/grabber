<?php declare(strict_types=1);

namespace App\Service;

use App\Repository\RepositoryInterface\FileRepositoryInterface;
use Exception;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class DownloadFileService
 * @package App\Service
 */
class FileService extends AbstractService
{
    private const FILE_EXTENSION_ZIP = 'zip';
    private const CONTENT_TYPE_HEADER = 'Content-Type: application/octet-stream';
    private const CONTENT_TYPE_DISPOSITION_PATTERN = 'Content-Disposition: attachment; filename=%s';
    private const CHECK_ZIP_EXTENSION_PATTERN = '(\.\.\/)';
    private const NEEDLE = '.';

    /**
     * @var Filesystem $filesystem
     */
    private $filesystem;

    /**
     * @var FileRepositoryInterface $repository
     */
    protected $repository;

    /**
     * DownloadFileService constructor.
     * @param Filesystem $filesystem
     * @param FileRepositoryInterface $repository
     */
    public function __construct(Filesystem $filesystem, FileRepositoryInterface $repository)
    {
        parent::__construct($repository);
        $this->filesystem = $filesystem;
    }

    /**
     * @param string $filename
     * @return bool
     * @throws Exception
     */
    public function downloadFile(string $filename): ?bool
    {
        $pathToFile = $this->repository->getFilePath($filename);
        if ($this->validateFile($filename, $pathToFile)) {
            header(self::CONTENT_TYPE_HEADER);
            header(sprintf(self::CONTENT_TYPE_DISPOSITION_PATTERN, $filename));

            exit(readfile($pathToFile));
        }

        return null;
    }

    /**
     * @param string $filename
     * @param string $pathToFile
     * @return bool
     */
    private function validateFile(?string $filename, ?string $pathToFile): bool
    {
        if ($pathToFile !== null) {
            $checkFileName = preg_match(self::CHECK_ZIP_EXTENSION_PATTERN, $pathToFile);
            $string = strrchr($filename, self::NEEDLE);
            if ($string) {
                return ($checkFileName || $this->filesystem->exists($pathToFile) ||
                    substr($string, 1) !== self::FILE_EXTENSION_ZIP);
            }
        }

        return false;
    }
}
