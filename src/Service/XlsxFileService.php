<?php declare(strict_types=1);

namespace App\Service;

use XLSXWriter;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class CsvFileGenerator
 * @package App\Service
 */
class XlsxFileService extends AbstractService
{
    public const REPORTS_FILE_PATH_PATTERN = '%s/reports/%s.xlsx';
    private const SHEET_ONE = 'SheetOne';

    /**
     * @var XLSXWriter $XLSXWriter;
     */
    private $XLSXWriter;

    /**
     * @var Filesystem $filesystem
     */
    private $filesystem;

    /**
     * CategoryService constructor.
     * @param XLSXWriter $XLSXWriter
     * @param Filesystem $filesystem
     */
    public function __construct(XLSXWriter $XLSXWriter, Filesystem $filesystem)
    {
        parent::__construct();
        $this->XLSXWriter = $XLSXWriter;
        $this->filesystem = $filesystem;
    }

    /**
     * @param array $headers
     * @return XLSXWriter
     */
    public function createFile(array $headers): XLSXWriter
    {
        $this->XLSXWriter->writeSheetHeader(self::SHEET_ONE, $headers);

        return $this->XLSXWriter;
    }

    /**
     * @param array $arrayForCSVFile
     * @return bool
     */
    public function writeDataToFile(array $arrayForCSVFile): bool
    {
        if (empty($arrayForCSVFile)) {
            return false;
        }

        foreach ($arrayForCSVFile as $row) {
            $this->XLSXWriter->writeSheetRow(self::SHEET_ONE, $row);
        }

        return true;
    }

    /**
     * @param string $fileName
     * @return void
     */
    public function saveFile(string $fileName): void
    {
        $directory = dirname($fileName);
        if (!$this->filesystem->exists($directory)) {
            $this->filesystem->mkdir($directory);
        }

        $this->XLSXWriter->writeToFile($fileName);
    }
}
