<?php declare(strict_types=1);

namespace App\Service;

use App\Exception\ZipFileGeneratorException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use ZipArchive;

/**
 * Class ZipFileService
 * @package App\Service
 */
class ZipFileService extends AbstractService
{
    public const REPORTS_ZIP_FILE_PATH_PATTERN = '%s/reports/%s.zip';
    private const FILE_NOT_FOUND_ERROR = 'File was not found';

    /**
     * @var ZipArchive $zipArchive
     */
    private $zipArchive;

    /**
     * @var Filesystem $filesystem
     */
    private $filesystem;

    /**
     * ZipFileService constructor.
     * @param ZipArchive $zipArchive
     * @param Filesystem $filesystem
     */
    public function __construct(ZipArchive $zipArchive, Filesystem $filesystem)
    {
        parent::__construct();
        $this->zipArchive = $zipArchive;
        $this->filesystem = $filesystem;
    }

    /**
     * @param string $pathToFile
     * @param string $zipFilePath
     * @return void
     * @throws ZipFileGeneratorException
     */
    public function generateZipArchive(string $pathToFile, string $zipFilePath): void
    {
        if (!$this->filesystem->exists($pathToFile)) {
            throw new ZipFileGeneratorException(self::FILE_NOT_FOUND_ERROR, Response::HTTP_NOT_FOUND);
        }

        $directory = dirname($pathToFile);

        if (!$this->filesystem->exists($directory)) {
            $this->filesystem->mkdir($directory);
        }

        $this->zipArchive->open($zipFilePath, ZipArchive::CREATE);
        $this->zipArchive->addFile($pathToFile, basename($pathToFile));
        $this->zipArchive->close();
    }
}
